#!/usr/bin/env python

import dominate
from dominate import tags
from dominate.util import raw
import markdown
import re
import yaml

def md(mdstring):
    #  https://stackoverflow.com/a/63866001/3302752
    return re.sub("(^<P>|</P>$)", "", markdown.markdown(mdstring, extensions=['extra']), flags=re.IGNORECASE)

langs = {}
entry_id = 0

def setup():
    #  TODO: Refactor this. I’m sure there are prettier ways to do this
    languages = list(yaml.load_all(open('database/languages.yaml'), Loader=yaml.SafeLoader))[0]
    mlwordles = list(yaml.load_all(open('database/multilingual.yaml'), Loader=yaml.SafeLoader))[0]
    domwordles = list(yaml.load_all(open('database/domain.yaml'), Loader=yaml.SafeLoader))[0]
    twistwordles = list(yaml.load_all(open('database/twist.yaml'), Loader=yaml.SafeLoader))[0]
    reimplwordles = list(yaml.load_all(open('database/reimplementations.yaml'), Loader=yaml.SafeLoader))[0]
    nonlingwordles = list(yaml.load_all(open('database/nonlinguistic.yaml'), Loader=yaml.SafeLoader))[0]
    miscwordles = list(yaml.load_all(open('database/misc.yaml'), Loader=yaml.SafeLoader))[0]
    nonwordles = list(yaml.load_all(open('database/non-wordly.yaml'), Loader=yaml.SafeLoader))[0]
    media = list(yaml.load_all(open('database/media.yaml'), Loader=yaml.SafeLoader))[0]
    media_other = list(yaml.load_all(open('database/media-other.yaml'), Loader=yaml.SafeLoader))[0]
    lists = list(yaml.load_all(open('database/lists.yaml'), Loader=yaml.SafeLoader))[0]
    global langs
    #  TODO: Refactor this. Use the structure of the YAML file directly; there is no need to convert it to this ugly form. This means changing tab_lang_eng() and tab_lang_native() as well
    for language in languages:
        langs[language['iso']] = {
            'name': language['native']['name'],
            'name-en': language['english']['name'],
            }
        if 'css' in language['native']:
            langs[language['iso']]['css'] = language['native']['css']
        if 'wiki' in language['native']:
            langs[language['iso']]['wiki'] = language['native']['wiki']['title']
            langs[language['iso']]['wiki-lang'] = language['native']['wiki']['lang']
            if 'incubator' in language['native']['wiki']:
                langs[language['iso']]['wiki-incubator'] = language['native']['wiki']['incubator']
        if 'wiki' in language['english']:
            langs[language['iso']]['wiki-en'] = language['english']['wiki']
    return mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles, miscwordles, media, nonwordles, lists, media_other

def wordlise(match_obj):
    output = ""
    for letter in match_obj.group(1):
        output += f'<div class="tile">{letter}</div>'
    return output

def toptext(wordles):
    def total_entries():
        """Counts the total number of entries, not including anchors (entry definition)"""
        nonlocal wordles
        notCounted = 0
        for lst in wordles:
            for entry in lst:
                if 'anchor' in entry and entry['anchor']:
                    notCounted += 1
        return str(sum(len(s) for s in wordles) - notCounted)
    global langs

    f = open('static/header.md', 'r').read()
    f = re.sub("{TOTAL}", total_entries(), f)
    f = re.sub("{LANGS}", str(len(langs)-1), f)
    f = re.sub("{LANGSLEFT}", str(8549-len(langs)-1), f)
    f = re.sub(r"{WORDLE}{([^}]*)}", wordlise, f)
    raw(markdown.markdown(f, extensions=['extra']))


def tab_lang_eng(wordle):
    with tags.td():
        with tags.span(cls="directlink"):
            tags.a("🔗",
                    href="#"+wordle['lang'])
            tags.span(" ")
        if 'wiki-en' in langs[wordle['lang']]:
            tags.a(
                    langs[wordle['lang']]['name-en'],
                    href="https://en.wikipedia.org/wiki/" + langs[wordle['lang']]['wiki-en'],
                    cls="lowkeylink",
                    id=wordle['lang']
                    )
        else:
            tags.span(
                    langs[wordle['lang']]['name-en'],
                    id=wordle['lang']
                    )

def tab_lang_native(wordle):
    cls = langs[wordle['lang']]['css'] if 'css' in langs[wordle['lang']] else ""
    if 'wiki' in langs[wordle['lang']]:
        if 'wiki-incubator' in langs[wordle['lang']] and langs[wordle['lang']]['wiki-incubator']:
            url = "https://incubator.wikimedia.org/wiki/Wp/" + langs[wordle['lang']]['wiki-lang'] + "/" + langs[wordle['lang']]['wiki']
        else:
            url = "https://" + langs[wordle['lang']]['wiki-lang'] + ".wikipedia.org/wiki/" + langs[wordle['lang']]['wiki']
        tags.td(tags.a(
            raw(langs[wordle['lang']]['name']),
            href=url,
            cls="lowkeylink"
            ), cls=cls)
    else:
        tags.td(langs[wordle['lang']]['name'], cls=cls)

def tab_lang(wordle, prevlang):
    global langs
    if prevlang != wordle['lang']:
        tab_lang_eng(wordle)
        tab_lang_native(wordle)
    else:
        tags.td()
        tags.td()

def tab_lang_only_english(wordle, prevlang):
    global langs
    if prevlang != wordle['lang']:
        tab_lang_eng(wordle)
    else:
        tags.td()

def tab_wordle(wordle):
    if 'css' in wordle:
        tags.td(tags.a(raw(wordle['name']), href=wordle['url']), cls=wordle['css'])
    else:
        tags.td(tags.a(wordle['name'], href=wordle['url']))

def tab_code(wordle):
    if 'src' in wordle:
        codeicons = {
            'github': 'github.svg',
            'git':    'git.svg',
            'glitch': 'glitch.svg',
            'gitlab': 'gitlab.svg',
            'pico-8': 'pico-8.png',
            'other':  'code.svg',
        }
        if 'src-type' in wordle and wordle['src-type'] in codeicons:
            codeicon = codeicons[wordle['src-type']]
        else:
            coderegexes = {
                'github': r'^https://github.com/',
                'glitch': r'^https://glitch.com/',
                'gitlab': r'^https://gitlab.com/',
                'pico-8': r'^https://www.lexaloffle.com/.+\.png$',
            }
            code = [k for k in coderegexes if re.compile(coderegexes[k]).search(wordle['src']) ]
            codeicon = codeicons[code[0]] if len(code) == 1 else codeicons['other']
        tags.td(tags.a(tags.img(src="gfx/code/"+codeicon, cls='icon code'), href=wordle['src']))
    else:
        tags.td()

def tab_note(wordle):
    with tags.td():
        if 'note' in wordle:
            tags.span(raw(md(wordle['note'])))
        meta_info(wordle)

def meta_info(wordle):
    global entry_id
    if any(t in wordle for t in ['developer', 'announcement', 'media']):
        with tags.span(style="float: right; margin-left: 1em"):
            tags.a("»", id=f"infosymbol-{entry_id}", style="font-weight: bold; cursor: pointer;",
                    onclick=f"$('#info-{entry_id}').toggle();")
        with tags.table(id=f"info-{entry_id}", cls="pure-table infotable", style="display: none"):
            if 'developer' in wordle:
                firstDeveloper = True
                for developer in wordle['developer']:
                    with tags.tr():
                        if firstDeveloper:
                            tags.td("🧑‍💻")
                        else:
                            tags.td()
                        with tags.td():
                            if 'url' in developer:
                                tags.a(developer['name'], href=developer['url'])
                            else:
                                tags.span(developer['name'])
                    firstDeveloper = False
            if 'announcement' in wordle:
                with tags.tr():
                    tags.td("🐣")
                    with tags.td():
                        if 'url' in wordle['announcement']:
                            tags.a(str(wordle['announcement']['date']), href=wordle['announcement']['url'])
                        else:
                            tags.span(str(wordle['announcement']['date']))
            if 'media' in wordle:
                with tags.tr():
                    tags.td("🗞")
                    with tags.td():
                        for medium in wordle['media']:
                            tags.a("🔗", href=medium['url'])

def mltable(wordles):
    global langs
    with tags.table(cls='pure-table mltable', id="ml"):
        tags.caption("🌐 Multilingual Wordle-like games; " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                with tags.th(colspan='2'):
                    tags.span("Language")
                    tags.span("🔗",
                            onclick="$('.directlink').toggle();",
                            style="float: right;")
                tags.th("Name")
                tags.th("Code")
                tags.th("Note")
        prevlang = ''
        with tags.tbody():
            for wordle in wordles:
                if not ('anchor' in wordle and wordle['anchor']):
                    with tags.tr():
                        tab_lang(wordle, prevlang)
                        prevlang = wordle['lang']
                        tab_wordle(wordle)
                        tab_code(wordle)
                        tab_note(wordle)
                    global entry_id
                    entry_id += 1

def eng_domain_specific(wordles):
    with tags.table(cls='pure-table domaintable', id="domain"):
        tags.caption("🔍 Domain-specific Wordle-like games; " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Language")
                tags.th()
                tags.th("Name")
                tags.th("Code")
                tags.th("Domain")
        prevlang = ''
        with tags.tbody():
            for wordle in wordles:
                if not ('anchor' in wordle and wordle['anchor']):
                    with tags.tr():
                        tab_lang_only_english(wordle, prevlang)
                        prevlang = wordle['lang']
                        tags.td(wordle['emoji'])
                        tab_wordle(wordle)
                        tab_code(wordle)
                        with tags.td():
                            tags.span(raw(md(wordle['domain'])))
                            meta_info(wordle)
                    global entry_id
                    entry_id += 1

def eng_twist(wordles, caption, htmlid):
    with tags.table(cls='pure-table twisttable', id=htmlid):
        tags.caption(caption + "; " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Language")
                tags.th()
                tags.th("Name")
                tags.th("Code")
                tags.th("Variation")
        prevlang = ''
        with tags.tbody():
            for wordle in wordles:
                if not ('anchor' in wordle and wordle['anchor']):
                    with tags.tr():
                        tab_lang_only_english(wordle, prevlang)
                        prevlang = wordle['lang']
                        tags.td(wordle['emoji'])
                        tab_wordle(wordle)
                        tab_code(wordle)
                        with tags.td():
                            tags.span(raw(md(wordle['twist'])))
                            meta_info(wordle)
                    global entry_id
                    entry_id += 1

def eng_reimplementations(wordles):
    with tags.table(cls='pure-table reimplementationtable', id="clones"):
        tags.caption("♻ Reimplementations of Wordle (English); " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Name")
                tags.th("Code")
                tags.th("Interface")
                tags.th("Development")
                tags.th("Note")
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    if not ('anchor' in wordle and wordle['anchor']):
                        tab_wordle(wordle)
                        tab_code(wordle)
                        tags.td(raw(md(wordle['interface'])))
                        tags.td(raw(md(wordle['dev'])))
                        tab_note(wordle)
                global entry_id
                entry_id += 1

def nonling(wordlesses):
    with tags.table(cls='pure-table nonlinguistictable', id="nonling"):
        tags.caption("😶 Non-linguistic wordles(s); " + str(len(wordlesses)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th()
                tags.th("Name")
                tags.th("Code")
                tags.th("Object of guessing")
        with tags.tbody():
            for wordless in wordlesses:
                with tags.tr():
                    if not ('anchor' in wordless and wordless['anchor']):
                        tags.td(wordless['emoji'])
                        tab_wordle(wordless)
                        tab_code(wordless)
                        with tags.td():
                            tags.span(raw(md(wordless['object'])))
                            meta_info(wordless)
                global entry_id
                entry_id += 1

def nonwordly(nonwordles):
    with tags.table(cls='pure-table nonwordlytable', id="nonwordly"):
        tags.caption("🟨 Related games with non-Wordle-like rules; " + str(len(nonwordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th()
                tags.th("Name")
                tags.th("Code")
                tags.th("Object")
                tags.th("Feedback")
                tags.th("Note")
        with tags.tbody():
            for wordle in nonwordles:
                if not ('anchor' in wordle and wordle['anchor']):
                    with tags.tr():
                        tags.td(wordle['emoji'])
                        tab_wordle(wordle)
                        tab_code(wordle)
                        tags.td(raw(md(wordle['object'])))
                        tags.td(raw(md(wordle['feedback'])))
                        tab_note(wordle)
                    global entry_id
                    entry_id += 1


def media(media, caption):
    with tags.table(cls='pure-table mediatable'):
        tags.caption(caption)
        with tags.thead():
            with tags.tr():
                tags.th("Publication")
                tags.th("Title")
                tags.th("Type")
                tags.th("Date")
        with tags.tbody():
            for medium in media:
                with tags.tr():
                    with tags.td():
                        tags.a(medium['publication']['name'],
                                href=medium['publication']['url'])
                    with tags.td():
                        tags.a(medium['title'],
                                href=medium['url'])
                    tags.td(medium['type'])
                    tags.td(str(medium['date']))

def lists(lists):
    with tags.h2(__pretty=False, id="others"):
        for letter in 'other':
            tags.div(letter, cls="tile")
    with tags.table(cls='pure-table liststable'):
        tags.caption("📜 Other lists of Wordle-like games; " + str(len(lists)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th("Coordinator")
                tags.th("Title")
                tags.th("Note")
                tags.th("Date")
        with tags.tbody():
            for lst in lists:
                with tags.tr():
                    with tags.td():
                        tags.a(lst['coordinator']['name'],
                                href=lst['coordinator']['url'])
                    with tags.td():
                        tags.a(lst['title'],
                                href=lst['url'])
                    tab_note(lst)
                    tags.td(str(lst['date']))

def misc(wordles):
    with tags.table(cls='pure-table misctable', id="misc"):
        tags.caption("⚙ Tools and miscellanea; " + str(len(wordles)) + " entries")
        with tags.thead():
            with tags.tr():
                tags.th()
                tags.th("Name")
                tags.th("Code")
                tags.th("Description")
        with tags.tbody():
            for wordle in wordles:
                with tags.tr():
                    tags.td(wordle['emoji'])
                    tab_wordle(wordle)
                    tab_code(wordle)
                    tags.td(raw(md(wordle['description'])))

def main():
    mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles, miscwordles, mediadata, nonwordles, listsdata, media_other = setup()
    doc = dominate.document(title='Wordles of the World')
    with doc.head:
        raw(open('static/head.html', 'r').read())
    with doc.body:
        with tags.div(cls='content-wrapper'):
            with tags.div(cls='content'):
                toptext([mlwordles, domwordles, twistwordles, reimplwordles, nonlingwordles, miscwordles, nonwordles])
                tags.hr()
                mltable(mlwordles)
                tags.p(raw(md("When a version is tagged ‘fraudulent’ it means it seems like trying to trick people into thinking it is the original Wordle. If we are wrong, [drop us a note](#write) and we will remove the tag.")))
                eng_domain_specific(domwordles)
                eng_twist(twistwordles, "🃏 Wordle-like games with a gameplay twist", "twist")
                eng_reimplementations(reimplwordles)
                nonling(nonlingwordles)
                misc(miscwordles)
                nonwordly(nonwordles)
                tags.hr()
                lists(listsdata)
                tags.hr()
                with tags.h2(__pretty=False, id="media"):
                    for letter in 'media':
                        tags.div(letter, cls="tile")
                media(mediadata, "📰 Media coverage of Wordles of the World; " + str(len(mediadata)) + " entries")
                media(media_other, "📰 Other related media articles; " + str(len(media_other)) + " entries")
                with tags.div(cls="footer"):
                    f = open('static/footer.md', 'r').read()
                    raw(markdown.markdown(f, extensions=['extra']))
    with doc:
        tags.script("twemoji.parse(document.body);")
    print(doc)

if __name__=="__main__":
    main()
